#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import json
import obspy
import struct
import socket
import serial
import signal
import argparse
import threading
import numpy as np
from io import BytesIO
from abc import ABCMeta, abstractmethod
from time import sleep
from select import select


PROG_NAME = os.path.basename(sys.argv[0])

OPEN_TIMEOUT = 10
NO_DATA_TIMEOUT = 30
BAUDRATE = 9600
PACKET_SIZE = 56
# MISALIGNMENT_THRESHOLD = 0.02
# MISALIGNMENT_THRESHOLD = 0.05
DATA_BURST_TIME_PERIOD = 0.9
DATA_TYPE = {
    'FLOAT': np.float64,
    'INT': np.int32
}


####################
# INPUT INTERFACES #
####################
class InputInterface(object):

    __metaclass__ = ABCMeta

    def __init__(self):
        self.__readable = None
        self.__buf = b''

    def _set_buf(self, buf):
        self.__buf = buf
        return self.__buf

    def _get_buf(self):
        return self.__buf

    def _set_readable(self, readable):
        self.__readable = readable
        return self.__readable

    def _get_readable(self):
        return self.__readable

    @abstractmethod
    def open(self):
        pass

    def close(self):
        self._get_readable().close()

    def flush(self):
        self._set_buf(b'')

    def seek(self, offset):
        return self._set_buf(self._get_buf()[offset:])

    def load_buf(self, size):
        buf = self._get_buf()
        while len(buf) < size:
            buf += self._get_readable().read(size - len(buf))
        return self._set_buf(buf)


class FileInputInterface(InputInterface):
    def __init__(self, filename):
        InputInterface.__init__(self)
        self.__filename = filename

    def open(self):
        f = open(self.__filename, 'rb')
        self._set_readable(f)
        return True
    
    def close(self):
        self._get_readable().close()


class SerialInputInterface(InputInterface):
    def __init__(self, port):
        InputInterface.__init__(self)
        self.__port = port

    def open(self):
        readable = self._set_readable(serial.Serial(
            port=self.__port,
            baudrate=BAUDRATE,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE
        ))
        if not readable.isOpen():
            readable.open()
        readable.write(b'*0100E4\r\n')
        return True

    def close(self):
        readable = self._get_readable()
        readable.write(b'*0100E3\r\n')
        readable.close()


class NetworkInterface(InputInterface):
    def __init__(self, address, port):
        InputInterface.__init__(self)
        self.__address = address
        self.__port = port

    def open(self):
        readable = self._set_readable(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
        readable.connect((self.__address, self.__port))
        readable.setblocking(0)
        sys.stderr.write('[%s] Connected to %s:%s\n' % (PROG_NAME, self.__address, self.__port))
        readable_list, _, _ = select([readable], [], [], NO_DATA_TIMEOUT)
        # if len(readable_list) == 0:
        #     sys.stderr.write('[%s] Timeout, reconnect in %s seconds...\n' % (PROG_NAME, OPEN_TIMEOUT))
        #     readable.close()
        #     sleep(OPEN_TIMEOUT)
        #     return False
        readable.send(b'*0100E4\r\n')
        return True

    def close(self):
        readable = self._get_readable()
        readable.send(b'*0100E3\r\n')
        readable.close()

    def load_buf(self, size):
        readable = self._get_readable()
        buf = self._get_buf()
        while len(buf) < size:
            try:
                chunk = readable.recv(size - len(buf))

            except socket.error as exception:
                readable_list, _, _ = select([readable], [], [], NO_DATA_TIMEOUT)
                if len(readable_list) == 0:
                    self.flush()
                    raise socket.timeout('[%s] Connection timed out' % (PROG_NAME))
                continue

            if len(chunk) == 0:
                self.flush()
                raise socket.timeout('[%s] Connection timed out' % (PROG_NAME))

            buf += chunk
        return self._set_buf(buf)


#####################
# OUTPUT INTERFACES #
####################
class OutputInterface(object):
    def __init__(self):
        self.__writable = None
        self.__stream = None
        self.__lock = threading.Lock()

    def _set_writable(self, writable):
        self.__writable = writable
        return self.__writable

    def _get_writable(self):
        return self.__writable

    def _get_lock(self):
        return self.__lock

    def open(self):
        pass

    def close(self):
        self._get_writable().close()

    def write(self, buf):
        writable = self._get_writable()
        lock = self._get_lock()
        try:
            lock.acquire()
            if len(buf) > 0:
                # sys.stderr.write('write %s bytes\n' % len(buf))
                writable.write(buf)
            writable.flush()
        finally:
            lock.release()


class SeedlinkInterface(OutputInterface):

    SEISCOMP_PLUGIN_PACKET_STRUCT = struct.Struct('i10s10siiiiiiiii512s')
    SEISCOMP_FD = 63

    def __init__(self):
        OutputInterface.__init__(self)

    def open(self):
        self._set_writable(os.fdopen(self.SEISCOMP_FD, 'wb'))

    def write(self, mseed):
        if len(mseed) == 0:
            return
        lock = self._get_lock()
        writable = self._get_writable()
        try:
            lock.acquire()
            for i in range(0, len(mseed), 512):
                station_code = mseed[i+8:i+13].strip()
                channel_code = mseed[i+15:i+18]
                # wrap mseed data in seedlink PluginPacket
                args = (
                    13,                   # 13 = PluginMSEEDPacket
                    station_code,         # station code
                    channel_code,         # channel code
                    0, 0, 0, 0, 0, 0,     # ptime (year, yday, hour, minute, seconf, usec)
                    0, 0, 512,            # (usec_correction, timing_quality, data_size)
                    bytes(mseed[i:i+512]) # data payload
                )
                plugin_packet = self.SEISCOMP_PLUGIN_PACKET_STRUCT.pack(*args)
                writable.write(plugin_packet)
                writable.flush()
        finally:
            lock.release()


class FileOutputInterface(OutputInterface):
    def __init__(self, filename):
        OutputInterface.__init__(self)
        self.__filename = filename

    def open(self):
        self._set_writable(open(self.__filename, 'ab'))


#################
# CLIENT THREAD #
#################
class ClientThread(threading.Thread):
    def __init__(self, name, must_die, config, input_interface=None, output_interfaces=[], raw_output=None, debug=False):
        threading.Thread.__init__(self, name=name)
        self.__must_die = must_die
        self.__config = config
        self.__input = input_interface
        self.__outputs = output_interfaces
        self.__raw_output = raw_output
        self.__data = dict()
        self.__debug = debug

    def _apply_polynomial_factors(self, device_id, measure_type, value: float):
        factors = self.__config[device_id]['channel'][measure_type]['polynomial_factors']
        dtype = self._get_data_type(device_id, measure_type)
        value = sum([pow(value, x) * y for x, y in enumerate(factors)])
        return int(value) if dtype == np.int32 else value
    
    def _get_sampling_rate(self, device_id):
        return self.__config[device_id]['sampling_rate']
    
    def _get_data_type(self, device_id, measure_type):
        return DATA_TYPE[self.__config[device_id]['channel'][measure_type]['data_type']]

    def _can_be_merged(self, prev_endtime, next_starttime, sampling_period):
        next_expected = prev_endtime + sampling_period
        abs_delta = abs((next_expected - next_starttime))
        if abs_delta < sampling_period / 2:
            # according to libmseed : default time tolerance is 1/2 the sampling period for continuous traces
            return True
        return False

    def _add_data(self, device_id, t, data):
        def create_timeserie(t, value):
            return { 'starttime': t, 'endtime': t, 'data': [value] }
        sampling_period = 1. / self._get_sampling_rate(device_id)
        if device_id not in self.__data:
            self.__data[device_id] = dict()
        for measure_type, value in data.items():
            if measure_type not in self.__data[device_id]:
                self.__data[device_id][measure_type] = [create_timeserie(t, value)]
            else:
                added = False
                for timeserie in self.__data[device_id][measure_type]:
                    if len(timeserie['data']) >= PACKET_SIZE:
                        continue
                    if self._can_be_merged(timeserie['endtime'], t, sampling_period):
                        timeserie['data'].append(value)
                        timeserie['endtime'] += sampling_period
                        added = True
                if not added:
                    self.__data[device_id][measure_type].append(create_timeserie(t, value))

    def _get_trace(self, net, sta, loc, cha, starttime, sampling_rate, data, data_type):
        return obspy.Trace(
            data=np.array(data, dtype=data_type),
            header={
                'starttime': starttime,
                'sampling_rate': sampling_rate,
                'network': net,
                'station': sta,
                'location': loc,
                'channel': cha
            }
        )
    
    def _log(self, msg):
        if self.__debug:
            sys.stderr.write(msg)
    
    def _print_timeseries(self, timeseries):
        if not self.__debug:
            return
        for i, ts in enumerate(timeseries):
            self._log(f'\t[{i}] {ts["starttime"]} - {ts["endtime"]} | {ts["data"]}\n')
    
    def _handle_data_burst(self):
        for device_id, measures in self.__data.items():
            sampling_period = 1. / self._get_sampling_rate(device_id)
            for measure_type, timeseries in measures.items():
                prev = None
                burst = list()
                for ts in timeseries:
                    if prev is None:
                        prev = ts
                        continue
                    if len(ts['data']) == 1 and abs(ts['starttime'] - prev['starttime']) < DATA_BURST_TIME_PERIOD:
                        self._log('data burst detected\n')
                        # data burst detected
                        burst.append(prev)
                    else:
                        prev = ts
                if len(burst) > 0:
                    self._log('before data burst handling\n')
                    self._print_timeseries(timeseries)
                    new_timeseries = list()
                    prev = None
                    for ts in timeseries:
                        if prev is None:
                            prev = ts
                            new_timeseries.append(ts)
                            continue
                        if ts in burst:
                            # append sample to previous timeserie
                            prev['data'].append(ts['data'][0])
                            prev['endtime'] += sampling_period
                        else:
                            if self._can_be_merged(prev['endtime'], ts['starttime'], sampling_period):
                                # merging previous timeserie with current one
                                prev['data'].extend(ts['data'])
                                prev['endtime'] += sampling_period * len(ts['data'])
                            else:
                                new_timeseries.append(ts)
                                prev = ts
                    self._log('after data burst handling\n')
                    self._print_timeseries(new_timeseries)
                    measures[measure_type] = new_timeseries

    def _get_mseed_packets_to_write(self, flush=False):
        to_write = obspy.Stream()
        for device_id, measures in self.__data.items():
            sampling_rate = self._get_sampling_rate(device_id)
            to_keep = dict()
            for measure_type in measures.keys():
                to_keep[measure_type] = list()
            for measure_type, timeseries in measures.items():
                dtype = self._get_data_type(device_id, measure_type)
                self._log('before writing\n')
                self._print_timeseries(timeseries)
                net, sta, loc, cha = self.__config[device_id]['channel'][measure_type]['name'].split('.')
                for ts in timeseries[:-2]:
                    tr = self._get_trace(net, sta, loc, cha, ts['starttime'],
                                                sampling_rate, ts['data'], dtype)
                    to_write += tr
                if flush or len(timeseries[-1]['data']) >= PACKET_SIZE:
                    for ts in timeseries[-2:]:
                        tr = self._get_trace(net, sta, loc, cha, ts['starttime'],
                                                    sampling_rate, ts['data'], dtype)
                        to_write += tr
                else:
                    for ts in timeseries[-2:]:
                        to_keep[measure_type].append(ts)
            for measure_type in measures.keys():
                measures[measure_type] = to_keep[measure_type]
                self._log('after writing\n')
                self._print_timeseries(measures[measure_type])
        if len(to_write) > 0:
            f = BytesIO()
            to_write.write(f, format='MSEED', reclen=512)
            return f.getvalue()
        return ''

    def run(self):
        while not self.__must_die.is_set():
            try:
                opened = self.__input.open()
                if not opened:
                    continue
                # t_prev = None
                while not self.__must_die.is_set():
                    # finding begining of packet
                    buf = self.__input.load_buf(3)
                    while buf != b'*00':
                        self.__input.seek(1)
                        buf = self.__input.load_buf(3)
                    l = len(buf)
                    while not buf.endswith(b'\r\n'):
                        l += 1
                        buf = self.__input.load_buf(l)
                    t = obspy.UTCDateTime()
                    try:
                        if self.__raw_output is not None:
                            self.__raw_output.write(f'{t} {buf.decode("utf-8")}'.encode('utf-8'))
                        device_id = buf[3:5].decode('utf-8')
                        ms, pressure, temperature = buf[6:-2].decode('utf-8').split(',')
                        self._log(f'{t} | device_id={device_id}, ms={ms}, pressure={pressure}, temperature={temperature}\n')

                        # compensate serial communication time
                        ms = int(ms) + len(buf) * 10 * 1e6 / BAUDRATE

                        data = {
                            'pressure': self._apply_polynomial_factors(device_id, 'pressure', float(pressure) * 100),# factor is applied relative to Pa (values come in mbar == hPa)
                            'temperature': self._apply_polynomial_factors(device_id, 'temperature', float(temperature))# factor is applied relative to degC
                        }
                        t -= ms * 1e-6
                        self._add_data(device_id, t, data)
                        self._handle_data_burst()
                        mseed = self._get_mseed_packets_to_write()

                        for output in self.__outputs:
                            output.write(mseed)
                        self.__input.flush()

                    except IOError as exception:
                        sys.stderr.write('[%s][%s] Failed to parse packet: %s\n' %
                                         (PROG_NAME, self.name, str(exception)))

            except IOError as exception:
                if not self.__must_die.is_set():
                    sys.stderr.write('\n'.join([
                        '[%s][%s] Connection lost : %s' % (PROG_NAME, self.name, str(exception)),
                        '[%s][%s] Wait %s seconds before reconnection...\n' % (PROG_NAME, self.name, OPEN_TIMEOUT)
                    ]))
                    sleep(OPEN_TIMEOUT)
                else:
                    break
            mseed = self._get_mseed_packets_to_write(flush=True)
            for output in self.__outputs:
                output.write(mseed)
        self.__input.close()


##################
# THREAD MANAGER #
##################
class ThreadManager(object):
    def __init__(self, config, input_interfaces, output_interfaces, raw_output, debug=False):
        self.__must_die = threading.Event()
        self.__must_die.clear()
        self.__pool = []
        self.__config = config
        self.__inputs = input_interfaces
        self.__outputs = output_interfaces
        self.__raw_output = raw_output
        self.__debug = debug

    def run(self):
        must_die = threading.Event()
        must_die.clear()

        def stop(signum, frame):
            must_die.set()

        signal.signal(signal.SIGTERM, stop)
        signal.signal(signal.SIGINT, stop)
        th_index = 0
        # open outputs
        for output in self.__outputs:
            output.open()
        for input_interface in self.__inputs:
            cth = ClientThread('Thread-%s' % th_index, must_die, self.__config,
                               input_interface=input_interface,
                               output_interfaces=self.__outputs,
                               raw_output=self.__raw_output,
                               debug=self.__debug)
            sys.stderr.write('[%s] Starting thread "%s"\n' % (PROG_NAME, cth.name))
            self.__pool.append(cth)
            cth.run()
            th_index += 1
        # close outputs
        for output in self.__outputs:
            output.close()
       #for cth in self.__pool:
       #    cth.join()
       #    sys.stderr.write('[%s] Thread "%s" has stopped\n' % (PROG_NAME, cth.name))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Utility tools for OSEAN Paroscientific Digiquartz Broadband Intelligent Instruments')
    parser.add_argument('name', metavar='NAME', help='Name used for logging')
    parser.add_argument('--config', help='Set configuration file.', required=True)
    parser_group_input = parser.add_argument_group('Input')
    parser_group_input.add_argument('--input-tcp', action='store_true', help='Uses the connection arguments in the config file.')
    parser_group_input.add_argument('--input-serial', help='Read on the given serial port.')
    parser_group_input.add_argument('--input-file', help='Read on the given file.')
    parser_group_output = parser.add_argument_group('Ouput')
    parser_group_output.add_argument('--output-file', help='Write MSEED data to given file.')
    parser_group_output.add_argument('--output-seiscomp', action='store_true', help='Send data to SeisComP3.')
    parser_group_output.add_argument('--output-raw', help='Write date and raw received packet to given file.')
    parser_group_output.add_argument('--debug', action='store_true', help='Enable debugging mode.')
    args = parser.parse_args()

    if args.name is not None:
        PROG_NAME = args.name

    input_interfaces = []
    output_interfaces = []

    with open(args.config, 'r') as f:
        config = json.load(f)

    # Configure input interfaces
    if args.input_serial is not None:
        input_interfaces.append(SerialInputInterface(port=args.input_serial))
    elif args.input_tcp:
        for device_id, device_attr in config.items():
            input_interfaces.append(
                NetworkInterface(device_attr['address'], device_attr['port'])
            )
    elif args.input_file:
        input_interfaces.append(FileInputInterface(args.input_file))

    # configure output interfaces
    if args.output_file is not None:
        output_interfaces.append(FileOutputInterface(filename=args.output_file))
    if args.output_seiscomp:
        output_interfaces.append(SeedlinkInterface())

    raw_output = None
    if args.output_raw:
        raw_output = FileOutputInterface(args.output_raw)
        raw_output.open()

    manager = ThreadManager(config, input_interfaces, output_interfaces, raw_output, args.debug)
    manager.run()
